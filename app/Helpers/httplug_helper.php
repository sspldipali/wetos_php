<?php

/**
* @author Dipali Patil
*/

use Http\Adapter\Guzzle7\Client as GuzzleAdapter;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request;

if ( !function_exists( 'makePostRequest' ) ) {
    function makePostRequest( $endpoint, $postBody, $headers = array( 'Accept' => 'application/json', 'Content-Type' => 'application/json' ) ) {
        return makeRequest( 'POST', $endpoint, $postBody, $headers );
    }
}

if ( !function_exists( 'makeGetRequest' ) ) {
    function makeGetRequest( $endpoint, $postBody, $headers = array( 'Accept' => 'application/json', 'Content-Type' =>
    'application/json' ) ) {
        return makeRequest( 'GET', $endpoint, $postBody, $headers );
    }
}

if ( !function_exists( 'makePutRequest' ) ) {
    function makePutRequest( $endpoint, $postBody, $headers = array( 'Accept' => 'application/json', 'Content-Type' =>
    'application/json' ) ) {
        return makeRequest( 'PUT', $endpoint, $postBody, $headers );
    }
}

if ( !function_exists( 'makeDeleteRequest' ) ) {
    function makeDeleteRequest( $endpoint, $postBody, $headers = array( 'Accept' => 'application/json', 'Content-Type' =>
    'application/json' ) ) {
        return makeRequest( 'DELETE', $endpoint, $postBody, $headers );
    }
}

function makeRequest( $method, $endpoint, $postBody, $headers = array( 'Accept' => 'application/json', 'Content-Type' => 'application/json' ) ) {

    $config = [
        'timeout' => 180, //seconds
        //'base_url' => env( 'BACKEND_URL' ),
        'debug' => false
    ];

    $guzzle = new GuzzleClient( $config );
    $adapter = new GuzzleAdapter( $guzzle );


    if ( Session::exists( 'key' ) ) {
        $headers['Authorization'] = 'Bearer ' . Session::get( 'key' );
    }
    // Returns a Psr\Http\Message\ResponseInterface
    Log::info( 'Calling ' . $method . ' API: ' . env( 'BACKEND_URL' ) . $endpoint );
    $request = new Request( $method, env( 'BACKEND_URL' ) . $endpoint, $headers, json_encode($postBody) );
    Log::info("REQ: " . $request->getUri());
    $response = $adapter->sendRequest( $request );
    Log::info("REQ: " . json_encode($response));
    return $response;
}
