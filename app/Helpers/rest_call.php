<?php


class RestResponse {
    public $status = 200;
    public $body = null;
    public $message = '';

    public static function dresponse( $statusCode, $body, $message = '' ) {
        if ( $statusCode >= 200 && $statusCode <= 299 ) {
            return response()->json(array(
                'success' => true,
                'message' => $message,
                'data' => $body
            ), 200 );
        } else {
            return response()->json(array(
                'success' => false,
                'message' => "Something went wrong"
            ), $statusCode );
        }
    }
}

if ( !function_exists( 'callAPI' ) ) {
    function callAPI( $method, $endpoint, $body, $headers ) {
        $restClient = env( 'REST_LIB', 'Unirest' );
        Log::info("Rest Client: " . $restClient);
        if ( $restClient == 'Unirest' ) {
            $response = null;
            switch( $method ) {
                case 'GET':
                $response = callGetAPI( $endpoint, $body, $headers );
                break;
                case 'POST':
                $response = postWithJsonBody( $endpoint, $body, $headers );
                //Log::info("Response==>".json_encode($response));
                break;
                case 'PUT':
                $response = putWithJsonBody( $endpoint, $body, $headers );
                break;
                case 'DELETE':
                $response = callDeleteAPI( $endpoint, $body, $headers );
                break;
                default:
                $response = postWithJsonBody( $endpoint, $body, $headers );
                break;
            }
            
            if ( $response != null ) {
                return RestResponse::dresponse($response->code, $response->body, "Request Processed Successfully!");
            }
        } else if ( $restClient == 'Httplug' ) {
            $response = null;
            switch( $method ) {
                case 'GET':
                $response = makeGetRequest( $endpoint, $body, $headers );
                break;
                case 'POST':
                $response = makePostRequest( $endpoint, $body, $headers );
                break;
                case 'PUT':
                $response = makePutRequest( $endpoint, $body, $headers );
                break;
                case 'DELETE':
                $response = makeDeleteRequest( $endpoint, $body, $headers );
                break;
                default:
                $response = makePostRequest( $endpoint, $body, $headers );
                break;
            }

            if ( $response != null ) {
                return RestResponse::dresponse($response->getStatusCode(), json_decode($response->getBody()->getContents()), "Request Processed Successfully!");
            }
        }
    }
}
