<?php 
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\ERPFrameworkController;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;

class DashboardController extends ERPFrameworkController
{

    /**
     * This method is used to show dashboard
     */
    public function index(Request $request)
    {
        try {
            $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
            $response = callAPI("GET","/api/auth/all/employee/",'',$headers);
            //$response = callAPI("GET","/api/auth/all/employee/",'',$headers);
            Log::info("response==>".json_encode($response));
            if($response->getStatusCode() == 200){
                $body = $response->getData();
                Log::info("res: " . json_encode($body));
                $this->data['employeeArray'] = $body->data->employee;
                Log::info("employeArr====>". json_encode($this->data['employeeArray']));
                //return View::make('dashboard.dashboard',$this->data);
            }else{
                $this->data['employeeArray'] = array();
            }
            $response1 = callAPI("GET","/api/auth/get-projects/",'',$headers);
            if($response1->getStatusCode() == 200){
                $body = $response1->getData();
                Log::info("res: " . json_encode($body));
                $this->data['projectArray'] = $body->data->project;
                Log::info("projectArray====>". json_encode($this->data['projectArray']));
               
            }else{
                $this->data['projectArray'] = array();
            }
            return View::make('dashboard.dashboard',$this->data);
        } catch (\Exception $e) {
            Log::info("Something went wrong! Exception: " . $e->getMessage());
            Log::error($e);
            $this->data['employeeArray'] = array();
            return View::make('dashboard.dashboard',$this->data);
        }
         
    }

    public function deleteEmployee(Request $request, $empId)
    {
        try {
            $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
            $response = callAPI("DELETE","/api/auth/delete/employee/" .$empId,'',$headers);
            Log::info("del response==>".json_encode($response));
            if($response->getStatusCode() == 200){
                return redirect("/dashboard");
            } else {                // set Flash  message
                return redirect("/dashboard");
            }
        } catch (\Exception $e) {
            Log::info("Something went wrong! Exception: " . $e->getMessage());
            Log::error($e);
            $this->data['employeeArray'] = array();
            return View::make('dashboard.dashboard',$this->data);
        }
         
    }

    public function getAllSessionData() {
        $sessionVal = Session::all();
        Log::info(json_encode($sessionVal));
        return response()->json(json_decode(json_encode(Session::all())));
    }


    /**
     * This method is used to show import page
     */
    public function import()
    {
        return View::make('dashboard.import');

    }

     /**
     * This method is used to show upload page
     */
    public function upload()
    {
        return View::make('dashboard.upload');

    }
        /**
     * This method is used to show upload document
     */
    public function uploadDocument()
    {
        return View::make('dashboard.upload_document');

    }

    /**
     * This method is used to show upload image
     */
    public function uploadImage()
    {
        return View::make('dashboard.upload_image');

    }
    
}