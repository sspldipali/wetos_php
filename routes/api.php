<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\GenericController;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/login/user', [LoginController::class, 'login']);
Route::post('/otp', [LoginController::class, 'getOTP']);
Route::post('/register', [LoginController::class, 'register']);
Route::get('/session' , [DashboardController::class, 'getAllSessionData']);

Route::post('/save/employee', [EmployeeController::class, 'saveEmployee']);
Route::post('/update/employee/{id}', [EmployeeController::class, 'updateEmployee']);
Route::get('/employee/{id}' , [EmployeeController::class, 'fetchEmployeeDetails']); 
Route::delete('/delete/employee/{id}' , [EmployeeController::class, 'deleteEmployee']); 

Route::any('/r/{ep}', [GenericController::class, 'executeAPI']);
Route::any('/r/{ep}/{ep1}', [GenericController::class, 'executeAPI']);
Route::any('/r/{ep}/{ep1}/{ep2}', [GenericController::class, 'executeAPI']);
Route::any('/r/{ep}/{ep1}/{ep2}/{ep3}', [GenericController::class, 'executeAPI']);
Route::any('/r/{ep}/{ep1}/{ep2}/{ep3}/{ep4}', [GenericController::class, 'executeAPI']);
Route::any('/r/{ep}/{ep1}/{ep2}/{ep3}/{ap4}/{ep5}', [GenericController::class, 'executeAPI']);
Route::any('/r/{ep}/{ep1}/{ep2}/{ep3}/{ap4}/{ep5}/{ep6}', [GenericController::class, 'executeAPI']);
