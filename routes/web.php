<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SellerController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\ManageRolesController;
use App\Http\Controllers\SettingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/signup', [LoginController::class, 'signup']);

Route::get('/', [LoginController::class, 'index']);
//Route::get('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout']);

//for dashboard view
Route::get('/dashboard', [DashboardController::class, 'index']);
Route::get('/delete/employee/{empId}', [DashboardController::class, 'deleteEmployee']);

//import functionality

Route::get('/import', [DashboardController::class, 'import']);

//upload functionality

Route::get('/upload', [DashboardController::class, 'upload']);

//upload functionality

Route::get('/upload-document', [DashboardController::class, 'uploadDocument']);

//upload functionality

Route::get('/upload-image', [DashboardController::class, 'uploadImage']);


//seller
//for dashboard view
Route::get('/seller', [SellerController::class, 'index']);

//import functionality

Route::get('/seller-import', [SellerController::class, 'import']);

//upload functionality

Route::get('/seller-upload', [SellerController::class, 'upload']);

//upload functionality

Route::get('/seller-upload-document', [SellerController::class, 'uploadDocument']);

//upload functionality

Route::get('/seller-upload-image', [SellerController::class, 'uploadImage']);

//Hotel
//for dashboard view
Route::get('/hotel', [HotelController::class, 'index']);

//import functionality

Route::get('/hotel-import', [HotelController::class, 'import']);

//upload functionality

Route::get('/hotel-upload', [HotelController::class, 'upload']);

//upload functionality

Route::get('/hotel-upload-document', [HotelController::class, 'uploadDocument']);

//upload functionality

Route::get('/hotel-upload-image', [HotelController::class, 'uploadImage']);

Route::get('/manage-roles', [ManageRolesController::class, 'manage_roles']);

Route::get('/setting', [SettingController::class, 'setting']);

