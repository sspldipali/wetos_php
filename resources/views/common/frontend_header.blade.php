<style>

    
h1.framework-app-name {
    margin-left: 8px;
    text-align: center !important;
}

.framework-container {
    display: flex;
    flex-direction: column;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
}

.b-color {
    max-height: 60px;
    background-color: rgb(6, 111, 197);
    color: white !important;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.field-error {
    color: #FF0000;
    font-size:85%;
}
</style>
<div class="framework-container">
        <nav class="navbar navbar-expand-lg navbar-dark b-color">
            <!-- <a class="nav-link waves-effect waves-light"> <i class="fas fa-bars"></i></a> -->
            <!-- Navbar brand -->
            <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>

            <a class="navbar-brand" href="#">Framework</a>
            <!--<a id="lnkLogout">Logout</a>-->
        </nav>
    </div>
