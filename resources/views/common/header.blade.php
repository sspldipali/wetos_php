
<link rel="stylesheet" href="{!! asset('theme_includes/css/dashboard/dashboard.css') !!}">
<style>
    .dropdown-menu.show {
    display: block;
    top: 110%;
    left: -142%;
}
.btn_ver{
    writing-mode:tb-rl;
    -webkit-transform:rotate(90deg);
    -moz-transform:rotate(90deg);
    -o-transform: rotate(90deg);
    -ms-transform:rotate(90deg);
    transform: rotate(90deg);
    color:#fff !important;
    font-size: 20px !important;
}
.btn-group, .btn-group-vertical {
    -webkit-box-shadow: 0 0px 0px 0 rgb(0 0 0 / 20%), 0 0px 0px 0 rgb(0 0 0 / 10%) !important;
    box-shadow: 0 0px 0px 0 rgb(0 0 0 / 20%), 0 0px 0px 0 rgb(0 0 0 / 10%)!important;
    border-radius: 3px;
   
}
.field-error {
    color: #FF0000;
    font-size:85%;
}

</style>
<div id="framework11"class="framework-container">
        <nav class="navbar navbar-expand-lg navbar-dark b-color">
            <!-- <a class="nav-link waves-effect waves-light"> <i class="fas fa-bars"></i></a> -->
            <!-- Navbar brand -->
          <div class="navbar_head_div">
            <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>

            <a class="navbar-brand" href="#">Framework</a>
            <!--<a id="lnkLogout">Logout</a>-->
         </div>
         <div>
           <div style="float:right !important;">
            <div class="btn-group ">
                <button type="button" class="btn dropdown-toggle btn_ver" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   ....
                </button>
                <div class="dropdown-menu">
                    <a href="{{url('/logout') }}"><button class="dropdown-item" type="button"> <i class="fas fa-sign-out-alt"></i> Logout</button></a>
                   
                </div>
            </div>
        </div>
        </div>

        </nav>
</div>
