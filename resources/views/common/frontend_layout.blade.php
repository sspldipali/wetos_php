<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PHP Framework - @yield('title')</title>
    <!-- MDB icon -->
    <link rel="icon" href="{!! asset('theme_includes/img/mdb-favicon.ico') !!}" type="image/x-icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
  
    <script src="{!! asset('theme_includes/js/jquery.min.js') !!}" type="text/javascript"></script>

    <link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{!! asset('theme_includes/css/bootstrap.min.css') !!}">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="{!! asset('theme_includes/css/mdb.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('theme_includes/css/signup/signup.css') !!}">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="{!! asset('theme_includes/css/style.css') !!}">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.12.0/jquery.validate.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.12.0/additional-methods.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js" integrity="sha512-90vH1Z83AJY9DmlWa8WkjkV79yfS2n2Oxhsi2dZbIv0nC4E6m5AbH8Nh156kkM7JePmqD6tcZsfad1ueoaovww==" crossorigin="anonymous"></script>
    <script src="{!! asset('js/utils.js') !!}" type="text/javascript"></script>
    <script type="text/javascript" >
      function preventBack(){window.history.forward();}
      setTimeout("preventBack()", 0);
      window.onunload=function(){null};
      //var baseURL = {!! json_encode(url('/')) !!};
      
    </script>
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
   <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
   <script src="{!! asset('js/lib/axios.min.js') !!}"></script>
   <script>
      const baseURL = "{{ url('/') }}";
      const jwtToken = sessionStorage.getItem("access_token");
       const dAuthAxios = axios.create({
           baseURL: `${baseURL}/api`,
           timeout: 90000,
           headers: {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${jwtToken}`}
        });
        //let session = JSON.parse(JSON.stringify(<?php //echo json_encode(Session::all()) ?>));
        //console.log("session: ", session);
        console.log("baseURL==>"+baseURL);
   </script>
   <style>
     .error-field, .highlight_error {
       color: "red !important"
     }
  </style>
@yield('scripts')
</head>
<body>
    
    @yield('content')
    @include('common.footer')
</body>
</html>
