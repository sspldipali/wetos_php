
<link rel="stylesheet" href="{!! asset('theme_includes/css/addons/datatables.min.css') !!}">
<link rel="stylesheet" href="{!! asset('theme_includes/css/dashboard/dashboard.css') !!}">
<!-- jQuery -->
<script type="text/javascript" src="{!! asset('theme_includes/js/jquery.min.js') !!}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{!! asset('theme_includes/js/popper.min.js') !!}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{!! asset('theme_includes/js/bootstrap.min.js') !!}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{!! asset('theme_includes/js/mdb.min.js') !!}"></script>
<!-- Your custom scripts (optional) -->
<!-- MDBootstrap Datatables  -->

<script type="text/javascript" src="{!! asset('theme_includes/js/addons/datatables.min.js') !!}"></script>
<!--<script type="text/javascript" src="{!! asset('theme_includes/js/addons/datatables2.min.js') !!}"></script>-->
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.12.0/jquery.validate.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.12.0/additional-methods.js"></script>
<!-- Your custom scripts (optional) -->
<script>
function openNav() {

    if( document.getElementById("mySidenav").className == "sidenav width_250") {
        var element = document.getElementById("mySidenav");
            element.classList.remove("width_250");
        var element1 = document.getElementById("main");
        element1.classList.remove("margin_left_250");
        var element2 = document.getElementById("mySidenav");
            element2.classList.add("width_0");
        var element3 = document.getElementById("main");
            element3.classList.add("margin_left_0");
        var element5 = document.getElementById("framework11");
            element5.classList.remove("back_gr");
    
    }else {
        var element = document.getElementById("mySidenav");
            element.classList.remove("width_0");
        var element2 = document.getElementById("main");
            element2.classList.remove("margin_left_0");
        var element3 = document.getElementById("mySidenav");
            element3.classList.add("width_250");
        var element4 = document.getElementById("main");
            element4.classList.add("margin_left_250");
        var element6 = document.getElementById("framework11");
            element6.classList.add("back_gr");

    }
}
</script>
<style>
    .width_250{
        width:250px ;
    }
    .width_0{
        width:0px ;
    }
    .margin_left_0{
        padding-left:0px ;
    }
    .margin_left_250{
        padding-left:250px ;
    }
    .navbar_head_div{
        width: 95%;display: flex;
    }
    @media all and (min-width: 0) and (max-width: 767px) {
        .width_250{
            width:250px;
            z-index:5;
        }
        .width_0{
            width:0px;
        }
        .margin_left_0{
            padding-left:0px !important;
        }
        .margin_left_250{
            padding-left:0px;
        }
    
        .back_gr{
            background: #0000007a;
            z-index: 5;;
        }
        .navbar_head_div{
            width: 80%;display: flex;
        }
    }
    
</style>