@extends('common.frontend_layout')
@section('title', 'Dashboard')
@section('scripts')
@include('common.header') 
@include('common.sidebar')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/mdb.min.css') !!}">
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>
<script src="{!! asset('js/dashboard-validator.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/utils.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/dashboard.js') !!}" type="text/javascript"></script>

<script>
    let projectRows = 0;
    let performanceDetailsRows = 0;
    let projectDetailsTable = null;
    let performanceDetailsTable = null;
    let isEdit = false;
    let isPerformanceEdit = false;
    let isProjectEdit = false;
    let selectedProjectRow = 0;
    let selectedPerformanceRow = 0;
    let employeeDetails = {};
</script>

<div id="main" class="mainDiv">
    <div class="margindiv" id="data-grid-container">
        <div class="datatbl">
            <div class="btn_e_i_d">
                <button type="button" class="btn btn-primary addbutton ripple-surface" id="btnShowEmployeeModal"> <!-- data-toggle="modal" data-target="#exampleModal" -->
                    <i class="fas fa-plus-circle "></i>
                </button>
                <a href ="#"><button id="" class="btn btn-primary btninport"><i class="fas fa-cloud-upload-alt"></i></button></a>
                <button id="exportTable" class="btn btn-primary btnexport"><i class="fas fa-file-export"></i></button>
            </div>
            <div class="table-responsive">
                <table id="table" class="display select" cellspacing="0" style="width:100%">
                    <thead>
                        <tr>
                            <th><input name="select_all" value="1" type="checkbox"></th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>Experience</th>
                            <th>Contact</th>
                            <th>Upload</th>
                            <th>Edit</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($employeeArray))
                        @foreach ($employeeArray as $empObj)
                        <tr id="1">
                            <td></td>
                            <td>{{ $empObj->name}}</td>
                            <td>{{ $empObj->designation}}</td>
                            <td>{{ $empObj->experience}}</td>
                            <td>{{ $empObj->contact}}</td>
                            <td><a href ="#"><button id="" class="btn btn-primary btnupload"><i class="fas fa-cloud-upload-alt"></i></button></a></td>
                            <td><button type="button" class="btn btn-primary btnpencil editEmp btnEdit" id="btnEdit{{ $loop->index }}" record-id="{{ $empObj->id }}"><i class="fas fa-pen"></i></button></td> <!-- data-toggle="modal" -->
                            <td><button id="button" class="btn btn-primary btntrasht btnRemoveEmployee" data-toggle="tooltip" title="Delete Employee" employee-id="{{ $empObj->id }}"><i class="fas fa-trash"></i></button></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="EmployeeDetailsModel" aria-hidden="true">
        <div class="modal-dialog  modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="firstblock">
                        <h5 class="bcolor" id="EmployeeDetailsModel">Employee Details</h5>
                        <form method="post" name="employeeForm" id="employeeFormID" style="margin-top:1%;">
                            <div class="row md-12 ">
                                <div class=" col-md-3">
                                    <div class="form-outline">
                                        <input type="hidden" name="id" id="id">
                                        <input type="text" id="empName" name="name" class="form-control" />
                                        <label class="form-label" for="empName">Name</label>
                                    </div>
                                    <span id="name_error" class="field-error"></span>
                                </div>
                                <div class=" col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empDesign" name="designation" class="form-control" />
                                        <label class="form-label" for="empDesign">Designation</label>
                                    </div>
                                    <span id="designation_error" class="field-error"></span>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empExp" name="experience" class="form-control" />
                                        <label class="form-label" for="empExp">Experience</label>
                                    </div>
                                    <span id="experience_error" class="field-error"></span>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empContact" name="contact" class="form-control" />
                                        <label class="form-label" for="empContact">Contact</label>
                                    </div>
                                    <span id="contact_error" class="field-error"></span>
                                </div>
                            </div>
                            <div id="employeeProjectDetails">
                            </div>
                            <div id="employeePerformanceDetails">
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Tabs navs -->
                <ul class="nav nav-tabs mb-3" id="ex1" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="ex1-tab-1" data-mdb-toggle="tab" href="#ex1-tabs-1" role="tab"
                            aria-controls="ex1-tabs-1" aria-selected="true">ProjectDetails</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="ex1-tab-2" data-mdb-toggle="tab" href="#ex1-tabs-2" role="tab"
                            aria-controls="ex1-tabs-2" aria-selected="false">PerformanceDetails</a>
                    </li>
                </ul>
                <!-- Tabs navs -->

                <!-- Tabs content -->
                <div class="tab-content" id="ex1-content">
                    <div class="tab-pane fade show active" id="ex1-tabs-1" role="tabpanel" aria-labelledby="ex1-tab-1">
                        <div class="table-responsive">
                        <div class="btn2_e2_i2_d2">
                            <button type="button" class="btn btn-primary addbutton11 addBtnForm" data-toggle="modal" id="btnShowAddProjectModal">
                                <i class="fas fa-plus-circle "></i>
                            </button>
                            <button id="exportProject" class="btn btn-primary btnexport11"><i class="fas fa-file-export"></i></button>                          
                        </div>
                            <table id="projectDetailsTb" class="display select table" cellspacing="0" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><input name="select_all" value="1" type="checkbox"></th>
                                        <th>ProjectCode</th>
                                        <th>ProjectName</th>
                                        <th>Technology</th>
                                        <th>Duration</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                       
                    </div>
                    <div class="secondblock">
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="EmployeeDetailsModel" aria-hidden="true">
                            <div class="modal-dialog  modal-md" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="bcolor">ProjectDetails</h5>
                                        <button type="button" class="close cancleBtnproject" data-dismiss="modal"
                                            aria-label="Close" id="btnCloseProjectDtl">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" name="addProjectForm" id="addProjectFormId">
                                            <div class="row md-12">
                                                <div class="col-md-6">
                                                    <label class="form-label" for="projectCode">Project Code*</label>
                                                    <div class="form-outline">
                                                        <input type="hidden" name="id" id="projectId">
                                                        <select class="mdb-select md-form select_form" id="projectCode" name="project_code">
                                                            @if(isset($projectArray))
                                                                <option value="">Select...</option>
                                                                @foreach($projectArray as $project)
                                                                 <option value="{{$project->project_code}}">{{ $project->project_code}}</option>
                                                                @endforeach

                                                            @endif
                                                        </select>
                                                    </div>
                                                    <span id="project_code_error" class="field-error"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label" for="projectName">Project Name*</label>
                                                    <div class="form-outline">
                                                        <select class="mdb-select md-form select_form" id="projectName" name="project_name">
                                                            @if(isset($projectArray))
                                                                <option value="">Select...</option>
                                                                @foreach($projectArray as $project)
                                                                 <option value="{{ $project->project_name}}">{{ $project->project_name}}</option>
                                                                @endforeach

                                                            @endif
                                                        </select>
                                                    </div>
                                                    <span id="project_name_error" class="field-error"></span>
                                                </div>
                                            </div>
                                            <div class="row md-12 md_space">
                                                <div class="col-md-6">
                                                    <label class="form-label" for="technology">Technology*</label>
                                                    <div class="form-outline">
                                                        <input type="text" id="technology" name="technology" class="form-control">
                                                    </div>
                                                    <span id="technology_error" class="field-error"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label" for="duration">Duration*</label>
                                                    <div class="form-outline">
                                                        <input type="text" id="duration" name="duration" class="form-control">
                                                    </div>
                                                    <span id="duration_error" class="field-error"></span>
                                                </div>
                                            </div>
                                            <div class="row mb-12 md_space">
                                                <div class="col-md-6">
                                                    <label class="form-label" for="status">Status *</label>
                                                    <div class="form-outline">
                                                        <select class="mdb-select md-form  select_form" id="status" name="status">
                                                            <option value="">Select...</option>
                                                            <option value="In-Progress">In-Progress</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                    </div>
                                                    <span id="status_error" class="field-error"></span>
                                                </div>
                                            </div>
                                            <div id="projectTblRows">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="btnAddProject">Save</button>
                                        <button type="button" class="btn btn-danger cancleBtnproject" id="btnCancel">Cancle</button> <!-- data-dismiss="modal" -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="EmployeeDetailsModel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="bcolor">PerformancetDetails</h5>
                                        <button type="button" class="close BtnCanclePer" id="btnClosePerformanceModal" aria-label="Close"> <!-- data-dismiss="modal" -->
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form name="performanceDetailsForm" id="performanceDetailsFormId">
                                        <div class="modal-body">
                                            <div class="row mb-12">
                                                <div class="col-sm-6">
                                                    <label class="form-label " for="year">Year*</label>
                                                    <div class="form-outline">
                                                        <input type="hidden" name="id" id="performanceId">
                                                        <input type="text" id="year" name="year" class="form-control">
                                                    </div>
                                                    <span id="year_error" class="field-error"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="form-label" for="grade">Grade*</label>
                                                    <div class="form-outline">
                                                        <select class="mdb-select md-form select_form" id="grade" name="grade">
                                                            <option value="">Select...</option>
                                                            <option value="Outstanding">Outstanding</option>
                                                            <option value="Commendable">Commendable</option>
                                                        </select>
                                                    </div>
                                                    <span id="grade_error" class="field-error"></span>
                                                </div>
                                            </div>
                                            <div class="row mb-12 md_space">
                                                <div class="col-sm-6">
                                                    <label class="form-label" for="comments">Comments*</label>
                                                    <div class="form-outline">
                                                        <input type="text" id="comments" name="comments" class="form-control">
                                                    </div>
                                                    <span id="comments_error" class="field-error"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="btnAddPerformanceDetails">Save</button>
                                        <button type="button" class="btn btn-danger BtnCanclePer" id="btnCancelPerformanceDetails">Cancle</button> <!-- data-dismiss="modal" -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="ex1-tabs-2" role="tabpanel" aria-labelledby="ex1-tab-2">
                        <div class="table-responsive">
                        <div class="btn3_e3_i3_d3">
                            <button type="button" class="btn btn-primary addbutton addBtnForm" id="btnShowPerformanceModal"> <!-- data-toggle="modal" data-target="#exampleModal3" -->
                                <i class="fas fa-plus-circle "></i>
                            </button>
                            <button id="exportPer" class="btn btn-primary btnexport"><i class="fas fa-file-export"></i></button>
                        </div>
                            <table id="performanaceDetailsTb" class="display select" cellspacing="0" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><input name="select_all" value="1" type="checkbox"></th>
                                        <th>Year</th>
                                        <th>Grade</th>
                                        <th>Comments</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
                <!-- Tabs content -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger cancleBtnMain" id="btnCloseEmployeeModal" data-dismiss="modal">
                        <i class="fas fa-times-circle"></i>&nbsp;Cancel
                    </button>
                    <div id="saveButtonsDiv">
                        <button type="button" class="btn btn-primary " id="btnSaveAndClose"><i class="fas fa-check-circle"></i>&nbsp;Save & Close</button>
                        <button type="button" class="btn btn-primary " id="btnSaveAndCreateNew"><i class="fas fa-plus-circle"></i>&nbsp;Save & New</button>
                    </div>
                    <div id="updateButtonsDiv">
                        <button type="button" class="btn btn-primary " id="btnUpdateEmployee"><i class="fas fa-check-circle"></i>&nbsp;Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop