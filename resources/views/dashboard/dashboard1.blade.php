@extends('common.frontend_layout')
@section('title', 'Dashboard')
@section('scripts')
@include('common.header') 
@include('common.sidebar')
<script src="{!! asset('js/dashboard.js') !!}" type="text/javascript"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/mdb.min.css') !!}">
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>

<script type="text/javascript">
let projectRows = 0;
let performanceRows = 0;
$(document).ready(function() {
    $("#exampleModal2").modal("hide");
    $("#lnkLogout").click(function() {
        sessionStorage.clear();
        window.location.href = "/logout";
    });
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    // $("#dt-all-checkbox").css("margin-left", "234px" , "width", "81%");
    $(".btnRemoveEmployee").click(function () {
        let empId = $(this).attr("employee-id");
       
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete the employee record!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete it!'
        }).then((result) => {
            window.location = `${baseURL}/delete/employee/${empId}`;
            /*
            $.ajax({
                "url": baseURL + '/api/delete/employee' + empId,
                "success": function(response) {
                    //Record delted call employee list API
                    fetchEmployeeList();
                }
            })
            */
        });
    });    
    
});

/*function fetchEmployeeList() {
    $.ajax({
        url: `${baseURL}/api/employee/list`,
        method: "GET",
        success: function(response) {
            // Clear datatable
            //Add rows to datatable
            response.employeeList.forEach((employee) => {
                $(`#emplTblId tbody`).append(`
                    <tr>
                        <td></td>
                    </tr>
                `);
            })
        }
    })
}*/

function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

$(document).ready(function() {
    // Array holding selected row IDs
    var rows_selected = [];
    var table = $('#table').DataTable({

        'ajax': '',
        'columnDefs': [{
            'targets': 0,
            'searchable': true,
            'orderable': false,
            'width': '1%',
            'className': 'dt-body-center',
            'render': function(data, type, full, meta) {
                return '<input type="checkbox">';
            }
        }],
        'order': [1, 'asc'],
        'rowCallback': function(row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            }
        }
    });

    // Handle click on checkbox
    $('#table tbody').on('click', 'input[type="checkbox"]', function(e) {

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes

    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on('click', function(e) {
        if (this.checked) {
            $('#table tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#table tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function() {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });

    // Handle form submission event 
    $('#frm-example').on('submit', function(e) {
        var form = this;

        // Iterate over all selected checkboxes
        $.each(rows_selected, function(index, rowId) {
            // Create a hidden element 
            $(form).append(
                $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'id[]')
                .val(rowId)
            );
        });
        // Output form data to a console     
        $('#example-console').text($(form).serialize());
        console.log("Form submission", $(form).serialize());

        // Remove added elements
        $('input[name="id\[\]"]', form).remove();

        // Prevent actual form submission
        e.preventDefault();
    });

});
</script>

<div id="main" class="mainDiv">
   
    <div class="margindiv" id="data-grid-container">
        <div class="datatbl">
            <div class="btn_e_i_d">
            <button type="button" class="btn btn-primary addbutton ripple-surface" data-toggle="modal"
                    data-target="#exampleModal">
                    <i class="fas fa-plus-circle "></i>
                </button>            
                <a href ="/import"><button id="" class="btn btn-primary btninport"><i class="fas fa-cloud-upload-alt"></i></button></a>
                <button id="exportTable" class="btn btn-primary btnexport"><i class="fas fa-file-export"></i></button>
            </div>
            <div class="table-responsive">
                <table id="table" class="display select" cellspacing="0" style="width:100%">
                    <thead>
                        <tr>
                            <th><input name="select_all" value="1" type="checkbox"></th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>Experience</th>
                            <th>Contact</th>
                            <th>Upload</th>
                            <th>Edit</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($employeeArray))
                        @foreach ($employeeArray as $empObj)
                        <tr id="1">
                            <td></td>
                            <td>{{ $empObj->name}}</td>
                            <td>{{ $empObj->designation}}</td>
                            <td>{{ $empObj->experience}}</td>
                            <td>{{ $empObj->contact}}</td>
                            <td><a href ="#"><button id="" class="btn btn-primary btnupload"><i
                                class="fas fa-cloud-upload-alt"></i></button></a></td>
                            <td><button type="button" class="btn btn-primary btnpencil editEmp" data-toggle="modal"><i
                                class="fas fa-pen"></i></button></td>
                            <td><button id="button" class="btn btn-primary btntrasht btnRemoveEmployee" data-toggle="tooltip" title="Delete Employee" employee-id="{{ $empObj->id }}"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                      
                    </tbody>

                </table>

            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="EmployeeDetailsModel"
        aria-hidden="true">
        <div class="modal-dialog  modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="firstblock">
                        <h5 class="bcolor" id="EmployeeDetailsModel">Employee Details</h5>
                        <form action="" method="post" id="employeeFormID">
                            <div class="row md-12 ">
                                <div class=" col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empName" name="name" class="form-control" />
                                        <label class="form-label" for="empName">Name</label>
                                    </div>
                                    <span id="empName-validation"></span>
                                </div>
                                <div class=" col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empDesign" name="designation" class="form-control" />
                                        <label class="form-label" for="empDesign">Designation</label>
                                    </div>
                                    <span id="design-validation"></span>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empExp" name="experience" class="form-control" />
                                        <label class="form-label" for="empExp">Experience</label>
                                    </div>
                                    <span id="exp-validation"></span>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empContact" name="contact" class="form-control" />
                                        <label class="form-label" for="empContact">Contact</label>
                                    </div>
                                    <span id="contact-validation"></span>
                                </div>
                            </div>
                            <div id="employeeProjectDetails">
                            </div>
                            <div id="employeePerformanceDetails">
                            </div>
                        </form>
                    </div>

                </div>

                <!-- Tabs navs -->
                <ul class="nav nav-tabs mb-3" id="ex1" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="ex1-tab-1" data-mdb-toggle="tab" href="#ex1-tabs-1" role="tab"
                            aria-controls="ex1-tabs-1" aria-selected="true">ProjectDetails</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="ex1-tab-2" data-mdb-toggle="tab" href="#ex1-tabs-2" role="tab"
                            aria-controls="ex1-tabs-2" aria-selected="false">PerformanceDetails</a>
                    </li>
                </ul>
                <!-- Tabs navs -->

                <!-- Tabs content -->
                <div class="tab-content" id="ex1-content">

                    <div class="tab-pane fade show active" id="ex1-tabs-1" role="tabpanel" aria-labelledby="ex1-tab-1">
                        <div class="btn2_e2_i2_d2">
                            <button type="button" class="btn btn-primary addbutton11 addBtnForm" data-toggle="modal" id="btnAddProject">
                                <i class="fas fa-plus-circle "></i>
                            </button>
                            <button id="exportProject" class="btn btn-primary btnexport11"><i
                                    class="fas fa-file-export"></i></button>                          
                        </div>
                        <div class="table-responsive">
                            <form id="addedProjectDetailsFormId">
                            <table id="projectDetailsTb" class="display select" cellspacing="0" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><input name="select_all" value="1" type="checkbox"></th>
                                        <th>ProjectCode</th>
                                        <th>ProjectName</th>
                                        <th>Technology</th>
                                        <th>Duration</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                        <th>Delete</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <!--<tr>
                                        <td></td>
                                        <td>PHP001</td>
                                        <td>Framework</td>
                                        <td>PHP</td>
                                        <td>1 month</td>
                                        <td>In-Progress</td>
                                        <td><button type="file" class="btn btn-primary btnpencil projectEditBtn"
                                                data-toggle="modal" onclick="editProjectTableRow();"><i
                                                    class="fas fa-pen"></i></button></td>
                                        <td><button id="" class="btn btn-primary btntrasht"><i
                                                    class="fas fa-trash"></i></button></td>
                                        <!-- <td>dtBasicExample2</td> -->
                                    </tr>
                                </tbody>

                            </table>
                            </form>
                        </div>
                    </div>
                    <div class="secondblock">
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog"
                            aria-labelledby="EmployeeDetailsModel" aria-hidden="true">
                            <div class="modal-dialog  modal-md" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="bcolor">ProjectDetails</h5>
                                        <button type="button" class="close cancleBtnproject" data-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="" method="post" id="projectFormID">
                                            <div class="row md-12">
                                                <div class="col-md-6">
                                                    <label class="form-label" for="projectCode">Project Code
                                                        *</label>
                                                    <div class="form-outline">
                                                        <select class="mdb-select md-form select_form" id="projectCode"
                                                            name="project_code">
                                                            @if(isset($projectArray))
                                                                <option value="">Select...</option>
                                                                @foreach($projectArray as $project)
                                                                 <option value="{{$project->project_code}}">{{ $project->project_code}}</option>
                                                                @endforeach

                                                            @endif
                                                        </select>
                                                    </div>
                                                    <span id="projectCode-validation"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label" for="projectName">Project Name
                                                        *</label>
                                                    <div class="form-outline">
                                                        <select class="mdb-select md-form select_form" id="projectName"
                                                            name="project_name">
                                                            @if(isset($projectArray))
                                                                <option value="">Select...</option>
                                                                @foreach($projectArray as $project)
                                                                 <option value="project_name">{{ $project->project_name}}</option>
                                                                @endforeach

                                                            @endif
                                                        </select>
                                                    </div>
                                                    <span id="projectName-validation"></span>
                                                </div>
                                            </div>

                                            <div class="row md-12 md_space">
                                                <div class="col-md-6">
                                                    <label class="form-label" for="technology">Technology
                                                        *</label>
                                                    <div class="form-outline">
                                                        <input type="text" id="technology" name="technology"
                                                            class="form-control">
                                                    </div>
                                                    <span id="technology-validation"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label" for="duration">Duration *</label>
                                                    <div class="form-outline">
                                                        <input type="text" id="duration" name="duration"
                                                            class="form-control">
                                                    </div>
                                                    <span id="duration-validation"></span>
                                                </div>
                                            </div>
                                            <div class="row mb-12 md_space">
                                                <div class="col-md-6">
                                                    <label class="form-label" for="status">Status *</label>
                                                    <div class="form-outline">
                                                        <select class="mdb-select md-form  select_form" id="status"
                                                            name="status">
                                                            <option value="">Select...</option>
                                                            <option value="In-Progress">In-Progress</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                    </div>
                                                    <span id="status-validation"></span>
                                                </div>
                                            </div>
                                            <div id="projectTblRows">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="BtnSave">Save</button>
                                        <button type="button" class="btn btn-secondary cancleBtnproject" id="BtnCancle"
                                            data-dismiss="modal">Cancle</button>
                                    </div>

                                </div>
                            </div>

                        </div>


                    </div>
                    <div class="tab-pane fade" id="ex1-tabs-2" role="tabpanel" aria-labelledby="ex1-tab-2">
                    <div class="btn3_e3_i3_d3">
                        <button type="button" class="btn btn-primary addbutton addBtnForm" data-toggle="modal"
                            data-target="#exampleModal3">
                            <i class="fas fa-plus-circle "></i>
                        </button>
                        <button id="exportPer" class="btn btn-primary btnexport"><i
                                class="fas fa-file-export"></i></button>
                                </div>
                        <!-- <button id="" class="btn btn-primary btninport"><i class="fas fa-cloud-upload-alt"></i></button> -->
                        <div class="table-responsive">
                            <table id="performanaceDetailsTb" class="display select" cellspacing="0" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><input name="select_all" value="1" type="checkbox"></th>
                                        <th>Year</th>
                                        <th>Grade</th>
                                        <th>Comments</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <!-- <tr>
                                        <td></td>
                                        <td>PHP001</td>
                                        <td>Framework</td>
                                        <td>comments</td>
                                      
                                        <td><button type="file" class="btn btn-primary btnpencil perEditBtn"
                                                data-toggle="modal"><i class="fas fa-pen"></i></button></td>
                                        <td><button id="" class="btn btn-primary btntrasht"><i
                                                    class="fas fa-trash"></i></button></td>
                                    </tr>-->
                                </tbody>

                            </table>

                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Tabs content -->

                <div class="modal-footer">

                    <button type="button" class="btn btn-danger cancleBtnMain" id="cancleBtn" data-dismiss="modal"><i
                            class="fas fa-times-circle"></i>&nbsp;cancle</button>

                    <button type="button" class="btn btn-primary " id="saveCloseBtn"><i
                            class="fas fa-check-circle"></i>&nbsp;Save & Close</button>

                    <button type="button" class="btn btn-primary " id="saveNewBtn"><i
                            class="fas fa-plus-circle"></i>&nbsp;Save & New</button>

                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="EmployeeDetailsModel"
        aria-hidden="true">
        <div class="modal-dialog  " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="bcolor">PerformancetDetails</h5>
                    <button type="button" class="close BtnCanclePer" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="performanceFormID">
                        <div class="row mb-12">
                            <div class="col-sm-6">
                                <label class="form-label " for="year">Year *</label>
                                <div class="form-outline">
                                    <input type="text" id="year" name="year" class=" select_form form-control">
                                </div>
                                <span id="year-validation"></span>
                            </div>

                            <div class="col-sm-6">
                                <label class="form-label" for="grade">Grade
                                    *</label>
                                <div class="form-outline">
                                    <select class="mdb-select md-form select_form" id="grade" name="grade">
                                        <option value="">Select...</option>
                                        <option value="Outstanding">Outstanding</option>
                                        <option value="Commendable">Commendable</option>
                                    </select>
                                </div>
                                <span id="grade-validation"></span>
                            </div>
                        </div>

                        <div class="row mb-12 md_space">
                            <div class="col-sm-6">
                                <label class="form-label " for="comments">Comments *</label>
                                <div class="form-outline">
                                    <input type="text" id="comments" name="comments" class=" select_form form-control">
                                </div>
                                <span id="comments-validation"></span>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-primary" id="BtnSavePer">Save</button>
                    <button type="button" class="btn btn-secondary BtnCanclePer" id="BtnCanclePer"
                        data-dismiss="modal">Cancle</button>
                </div>
            </div>

        </div>
    </div>
    <!-- edit functionality -->
    <div class="modal fade" id="empModal" tabindex="-1" role="dialog" aria-labelledby="EmployeeDetailsModel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="firstblock">
                        <h5 class="bcolor" id="EmployeeDetailsModelEdit">Employee Details</h5>
                        <form>
                            <div class="row md-12 ">
                                <div class="lg col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empNameEdit" name="empNameEdit"
                                            class="form-control editBorder" />
                                        <label class="form-label lbl" for="empNameEdit">Name</label>
                                    </div>
                                    <span id="empNameEdit-validation"></span>
                                </div>
                                <div class=" col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empDesignEdit" name="empDesignEdit"
                                            class="form-control editBorder" />
                                        <label class="form-label lbl" for="empDesignEdit">Designation</label>
                                    </div>
                                    <span id="designEdit-validation"></span>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empExpEdit" name="empExpEdit"
                                            class="form-control editBorder" />
                                        <label class="form-label lbl" for="empExpEdit">Experience</label>
                                    </div>
                                    <span id="expEdit-validation"></span>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-outline">
                                        <input type="text" id="empContactEdit" name="empContactEdit"
                                            class="form-control editBorder" />
                                        <label class="form-label lbl" for="empContactEdit">Contact</label>
                                    </div>
                                    <span id="contactEdit-validation"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="editPer" tabindex="-1" role="dialog"
                        aria-labelledby="EmployeeDetailsModel" aria-hidden="true">
                        <div class="modal-dialog  " role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="bcolor">PerformancetDetails</h5>
                                    <button type="button" class="close cancleEditPer" data-dismiss="modal"
                                        aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="row mb-12">
                                            <div class="col-sm-6">
                                                <label class="form-label " for="yearEdit">Year *</label>
                                                <div class="form-outline">
                                                    <input type="text" id="yearEdit" name="yearEdit"
                                                        class=" select_form form-control">
                                                </div>
                                                <span id="yearEdit-validation"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="form-label" for="gradeEdit">Grade
                                                    *</label>
                                                <div class="form-outline">
                                                    <select class="mdb-select md-form select_form" id="gradeEdit"
                                                        name="gradeEdit">
                                                        <option value="">Select...</option>
                                                        <option value="Grade 1">Grade 1</option>
                                                        <option value="Grade 2">Grade 2</option>
                                                    </select>
                                                </div>
                                                <span id="gradeEdit-validation"></span>
                                            </div>
                                        </div>

                                        <div class="row mb-12 md_space">
                                            <div class="col-sm-6">
                                                <label class="form-label " for="commentsEdit">Comments *</label>
                                                <div class="form-outline">
                                                    <input type="text" id="commentsEdit" name="commentsEdit"
                                                        class=" select_form form-control">
                                                </div>
                                                <span id="commentsEdit-validation"></span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="BtnSavePerEdit">Save</button>
                                    <button type="button" class="btn btn-secondary cancleEditPer" id="BtnCanclePerEdit"
                                        data-dismiss="modal">Cancle</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="secondblock">
                        <!-- Modal -->
                        <div class="modal fade" id="editProject" tabindex="-1" role="dialog"
                            aria-labelledby="EmployeeDetailsModel" aria-hidden="true">
                            <div class="modal-dialog  modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="bcolor">ProjectDetails</h5>
                                        <button type="button" class="close cancleEmpModal" data-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="row md-12">
                                                <div class="col-md-6">
                                                    <label class="form-label" for="projectCodeEdit">Project Code
                                                        *</label>
                                                    <div class="form-outline">
                                                        <select class="mdb-select md-form select_form"
                                                            id="projectCodeEdit" name="projectCodeEdit">
                                                            <option value="">Select...</option>
                                                            <option value="PHP001">PHP001</option>
                                                            <option value="Project 2">Project 2</option>
                                                        </select>
                                                    </div>
                                                    <span id="projectCodeEdit-validation"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label" for="projectNameEdit">Project Name
                                                        *</label>
                                                    <div class="form-outline">
                                                        <select class="mdb-select md-form select_form"
                                                            id="projectNameEdit" name="projectNameEdit">
                                                            <option value="">Select...</option>
                                                            <option value="Framework">Framework</option>
                                                            <option value="2">Project 2</option>
                                                        </select>
                                                    </div>
                                                    <span id="projectNameEdit-validation"></span>
                                                </div>
                                            </div>

                                            <div class="row md-12 md_space">
                                                <div class="col-md-6">
                                                    <label class="form-label" for="technologyEdit">Technology
                                                        *</label>
                                                    <div class="form-outline">
                                                        <input type="text" id="technologyEdit" name="technologyEdit"
                                                            class="form-control">
                                                    </div>
                                                    <span id="technologyEdit-validation"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label" for="durationEdit">Duration *</label>
                                                    <div class="form-outline">
                                                        <input type="text" id="durationEdit" name="durationEdit"
                                                            class="form-control">
                                                    </div>
                                                    <span id="durationEdit-validation"></span>
                                                </div>
                                            </div>
                                            <div class="row mb-12 md_space">
                                                <div class="col-md-6">
                                                    <label class="form-label" for="statusEdit">Status *</label>
                                                    <div class="form-outline">
                                                        <select class="mdb-select md-form  select_form" id="statusEdit"
                                                            name="statusEdit">
                                                            <option value="">Select...</option>
                                                            <option value="In-Progress">In Progress</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                    </div>
                                                    <span id="statusEdit-validation"></span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="BtnSaveEdit">Save</button>
                                        <button type="button" class="btn btn-secondary cancleEmpModal"
                                            id="BtnCancleEdit" data-dismiss="modal">Cancle</button>
                                    </div>

                                </div>
                            </div>

                        </div>


                    </div>
                </div>

                <!-- Tabs navs -->
                <ul class="nav nav-tabs mb-3" id="ex1Edit" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="ex1-tab-3" data-mdb-toggle="tab" href="#ex1-tabs-3" role="tab"
                            aria-controls="ex1-tabs-1" aria-selected="true">ProjectDetails</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="ex1-tab-4" data-mdb-toggle="tab" href="#ex1-tabs-4" role="tab"
                            aria-controls="ex1-tabs-4" aria-selected="false">PerformanceDetails</a>
                    </li>
                </ul>
                <!-- Tabs navs -->

                <!-- Tabs content -->
                <div class="tab-content" id="ex1-contentEdit">

                    <div class="tab-pane fade show active" id="ex1-tabs-3" role="tabpanel" aria-labelledby="ex1-tab-3">
                        <div class="btn1_e1_i1_d1">
                            <button type="button" class="btn btn-primary addbutton11 addBtnForm" data-toggle="modal"
                                data-target="#editProject">
                                <i class="fas fa-plus-circle "></i>
                            </button>
                            <button id="exportEditProject" class="btn btn-primary btnexport11"><i
                                    class="fas fa-file-export"></i></button>
                            <!-- <button id="" class="btn btn-primary btninport11"><i
                                    class="fas fa-cloud-upload-alt"></i></button> -->
                        </div>

                        <div class="table-responsive">
                            <table id="dtBasicExample2Edit" class="table  table-bordered table-md" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th><input name="select_all" value="1" type="checkbox"></th>
                                        <th>ProjectCode</th>
                                        <th>ProjectName</th>
                                        <th>Technology</th>
                                        <th>Duration</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                        <th>Delete</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>PHP001</td>
                                        <td>Framework</td>
                                        <td>PHP</td>
                                        <td>1 month</td>
                                        <td>In-Progress</td>
                                        <td><button type="file" class="btn btn-primary btnpencil projectEdit"
                                                data-toggle="modal"><i class="fas fa-pen"></i></button></td>
                                        <td><button id="" class="btn btn-primary btntrasht"><i
                                                    class="fas fa-trash"></i></button></td>
                                        <!-- <td>dtBasicExample2</td> -->
                                    </tr>
                                </tbody>

                            </table>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="ex1-tabs-4" role="tabpanel" aria-labelledby="ex1-tab-4">
                        <div class="btn1_e1_i1_d1"> <button type="button" class="btn btn-primary addbutton11 addBtnForm"
                                data-toggle="modal" data-target="#editPer">
                                <i class="fas fa-plus-circle "></i>
                            </button>
                            <button id="exportEditPer" class="btn btn-primary btnexport11"><i
                                    class="fas fa-file-export"></i></button>
                            <!-- <button id="" class="btn btn-primary btninport11"><i
                                    class="fas fa-cloud-upload-alt"></i></button> -->
                        </div>
                        <div class="table-responsive">
                            <table id="dtBasicExample3" class="table  table-bordered table-md" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th><input name="select_all" value="1" type="checkbox"></th>
                                        <th>Year</th>
                                        <th>Grade</th>
                                        <th>Comments</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
                <!-- Tabs content -->

                <div class="modal-footer">

                    <button type="button" class="btn btn-danger cancleEditMain" id="cancleBtnEdit"
                        data-dismiss="modal"><i class="fas fa-times-circle"></i>&nbsp;cancle</button>

                    <button type="button" class="btn btn-primary " id="saveCloseBtnEdit"><i
                            class="fas fa-check-circle"></i>&nbsp;Save & Close</button>

                    <button type="button" class="btn btn-primary " id="saveNewBtnEdit"><i
                            class="fas fa-plus-circle"></i>&nbsp;Save & New</button>
                </div>


            </div>
        </div>
    </div>
</div>
<!-- End your project here-->

@stop