@extends('common.frontend_layout')
@section('title', 'Upload_Document')
@section('scripts')
@include('common.header')
@include('common.sidebar')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<link rel="stylesheet" href="{!! asset('theme_includes/css/mdb.min.css') !!}">
<link rel="stylesheet" href="{!! asset('theme_includes/css/dashboard/upload_document.css') !!}">
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.3.1/flatly/bootstrap.min.css">

<!-- Font Awesome CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript">
    function readUrl(input) {
  
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = (e) => {
      let imgData = e.target.result;
      let imgName = input.files[0].name;
      input.setAttribute("data-title", imgName);
      console.log(e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }

}
</script>
<style>

</style>
@stop
@section('content')


<div id="main"class="mainDiv">
    <div class="container">
        <div class="row margin_top_7">
            <div class="col-sm-10 offset-sm-2">
            <a href ="/upload"><button type="button" class="btn btn-light btn_back"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
            </div>
        </div>
        <div class="row margin_top_2" >
            <div class="col-sm-10 offset-sm-2" >
            <div class="bulkimport_container">
            
                <div class="form-group inputDnD">
                <button type="button" class="btn btn-default btn-block txt_color" onclick="document.getElementById('inputFile').click()">Bulk Import Excel Sheet</button>
                    <label class="sr-only" for="inputFile">File Upload <i class="fas fa-cloud-upload-alt"></i></label>
                    <input type="file" class="form-control-file txt_color font-weight-bold " id="inputFile" accept=".doc,.pdf,.rtf,.docx" onchange="readUrl(this)" data-title="Drag and drop a file">
                </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- End your project here-->

@stop