@extends('common.login_layout')
@section('title', 'Login | SignUp')
@section('content')
<script src="{!! asset('js/login.js') !!}" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/login/login.css') !!}">

<style>
 .validation
{
    color: red;
    font-size: 12px;
    
}
</style>
<script type="text/javascript">
    //const togglePassword = document.querySelector('#togglePassword');
    const togglePassword = document.getElementById('togglePassword');
    const password = document.getElementById('UserForm-pass');
     

    $(document).ready(function() {
       
    });
 

    function pageRedirect() {
        // window.navigate("htmlcode.html");
    }     
</script>

 <div class="container">
        <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
            <div class="card-body login-details">
                <form action="login" method="POST">
                <div class="form-header">
                    <h3 class="login-header-style">Login</h3>
                </div>
                <div class="form-outline position1">
                    <i class="fas fa-user   position2"></i>
                    <input type="text"id="userName" name="username" class="form-control">
                    <label for="username" class="form-label">User name</label>
                </div>
                <span id="username-validation" class="validation"></span>
                <div class="form-outline position1">
                    <i class="fas fa-eye-slash  position2" id="togglePassword"></i>
                    <input type="password" id="password" name="password" class="form-control"  >
                    <label for="UserForm-pass" class="form-label">Your password</label>
                </div>
                <span id="password-validation" class="validation"></span>
                <br>
                              
                <fieldset class="rememberMe-container">
                    <div class=" rememberMe-style">
                        <input color="primary" type="checkbox" id="remember_me" 
                        name="remember_me"  value="remember-me">
                        <label class="" for="checkbox2">Remember Me</label>
                    </div>
                </fieldset>
                <div class="text-center">
                    <button name="submit" type="button" id="btnLogin" class="login-button"><i class="fas fa-lock "></i>&nbsp;Login</button>
                      <a href="{{url('/signup')}}"><button class="signup-button" type="button"><i
                        class="fas fa-user-plus "></i>&nbsp;Create a New Account</button></a>
                </div>
                </form>
            </div>
        </div>
    </div>
<!-- End your project here-->
@stop
